/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.russian;



import android.app.Activity;
import android.app.ActionBar;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Displays a word and its definition.
 */
public class WordActivity extends Activity {
	private TextView word;
	private TextView definition;
	FavDatabase favdatabase;
	Uri uri;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.word);
		word = (TextView) findViewById(R.id.word);
		definition = (TextView) findViewById(R.id.definition);
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.wordmenu, menu);

		return true;
	}



	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		favdatabase = new FavDatabase(this);
		
		uri = getIntent().getData();
		Cursor cursor = managedQuery(uri, null, null, null, null);
		if (cursor == null) {
			finish();
		} else {
			cursor.moveToFirst();
			int wIndex = cursor.getColumnIndexOrThrow(DictionaryDatabase.KEY_WORD);
			int dIndex = cursor.getColumnIndexOrThrow(DictionaryDatabase.KEY_DEFINITION);
			//originalID = cursor.getColumnIndexOrThrow( "rowid AS " +
					//BaseColumns._ID);
			word.setText(cursor.getString(wIndex));
			definition.setText(cursor.getString(dIndex));
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.fav:
			long temp = favdatabase.addWord(word.getText().toString(),definition.getText().toString(),uri);
			if( temp == -1){
				Toast.makeText(this, "お気に入り登録できませんでした", Toast.LENGTH_SHORT).show();
			} else if(temp == -2){
				Toast.makeText(this, "すでに登録されています", Toast.LENGTH_SHORT).show();
			}
			return true;
		case R.id.settings:
			Intent intent2 = new Intent(this, SettingsActivity.class);
			intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent2);
		default:
			return false;
		}
	}
}
