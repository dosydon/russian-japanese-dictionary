package com.example.russian;


import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;


/**
 * Contains logic to return specific words from the dictionary, and
 * load the dictionary table when it needs to be created.
 */
public class FavDatabase {
	private static final String TAG = "FavDatabase";

	//The columns we'll include in the dictionary table
	public static final String KEY_WORD = "KEY_WORD";
	public static final String KEY_DEFINITION = "KEY_DEFINITION";
	public static final String COLUMN_ID = "_id";
	public static final String URI = "URI";

	private static final String DATABASE_NAME = "fav";
	private static final int DATABASE_VERSION = 1;
	private String[] allColumns = { COLUMN_ID,
			KEY_WORD, KEY_DEFINITION, URI };
	final DictionaryOpenHelper mDatabaseOpenHelper;
	private SQLiteDatabase writable;
	/**
	 * Constructor
	 * @param context The Context within which to work, used to create the DB
	 */
	public FavDatabase(Context context) {
		mDatabaseOpenHelper = new DictionaryOpenHelper(context);
		writable = mDatabaseOpenHelper.getWritableDatabase();
	}

	public Cursor getAll() {
		Cursor cursor = writable.query(DATABASE_NAME,
				allColumns, null, null, null, null, null);

		if (cursor == null) {
			return null;
		} else if (!cursor.moveToFirst()) {
			cursor.close();
			return null;
		}
		return cursor;
	}

	/**
	 * Add a word to the dictionary.
	 * @return rowId or -1 if failed
	 */
	public long addWord(String word, String definition, Uri uri) {
		ContentValues initialValues = new ContentValues();
		initialValues.put(KEY_WORD, word);
		initialValues.put(KEY_DEFINITION, definition);
		initialValues.put(URI, uri.toString());
		Cursor cursor = writable.query(DATABASE_NAME,
				allColumns, KEY_WORD + " = "+"'"+word+"'", null,
				null, null, null);
		if(cursor == null)
			return -3;

		if(cursor.getCount() > 0){
			cursor.close();
			return -2;
		} else{
			cursor.close();
			return writable.insert(DATABASE_NAME, null, initialValues);
		}

	}

	int deleteAll(){
		return writable.delete(DATABASE_NAME, null, null);
	}
	
	int deleteWord(long id){
		return writable.delete(DATABASE_NAME, COLUMN_ID + " = "+ id, null);
	}

	/**
	 * This creates/opens the database.
	 */
	class DictionaryOpenHelper extends SQLiteOpenHelper {

		//private final Context mHelperContext;
		private SQLiteDatabase mDatabase;

		private static final String DATABASE_CREATE = "create table "
				+ DATABASE_NAME + "(" + COLUMN_ID
				+ " integer primary key autoincrement, " + KEY_WORD
				+ " text not null"+","+KEY_DEFINITION+" text not null"+","+URI+" text not null"+");";

		DictionaryOpenHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			mDatabase = db;
			mDatabase.execSQL(DATABASE_CREATE);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			
		}
	}

}