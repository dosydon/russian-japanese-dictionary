
package com.example.russian;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

/**
 * Displays a word and its definition.
 */
public class FavActivity extends Activity {
	FavDatabase favdatabase;
	ListView listView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fav);
		listView = (ListView)findViewById(R.id.ListView);	
		if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
			ActionBar actionBar = getActionBar();
			actionBar.setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.favmenu, menu);

		return true;
	}



	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		favdatabase = new FavDatabase(this);
		setListView();
	}

	public void setListView(){
		//お気に入り全データを取得して、ListViewにセット
		final Cursor cursor = favdatabase.getAll();
		if (cursor == null) {
			//finish();
		} else {
			// Specify the columns we want to display in the result
			String[] from = new String[] { FavDatabase.KEY_WORD,
					FavDatabase.KEY_DEFINITION };

			// Specify the corresponding layout elements where we want the columns to go
			int[] to = new int[] { R.id.word,
					R.id.definition };

			// Create a simple cursor adapter for the definitions and apply them to the ListView
			SimpleCursorAdapter words = new SimpleCursorAdapter(this,
					R.layout.result, cursor, from, to);
			listView.setAdapter(words);

			listView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					Intent wordIntent = new Intent(getApplicationContext(), WordActivity.class);
					cursor.moveToFirst();
					for(int i = 0; i < position;i++){
						cursor.moveToNext();
					}
					int uriIndex = cursor.getColumnIndexOrThrow(FavDatabase.URI);
					String uri = cursor.getString(uriIndex);
					Uri data = Uri.parse(uri);
					wordIntent.setData(data);
					startActivity(wordIntent);
				}
			});
			registerForContextMenu(listView);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			Intent intent = new Intent(this, MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		case R.id.clear:
			//選択項目を準備する。
			String[] str_items = {
					"OK",
			"キャンセル"};

			new AlertDialog.Builder(FavActivity.this)
			.setTitle("お気に入りデータを全消去しますか？")
			.setItems(str_items, new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {

					//選択したアイテムの番号(0～)がwhichに格納される
					switch(which)
					{
					case 0:
						// OK
						favdatabase.deleteAll();
						Intent intent1 = new Intent(FavActivity.this, FavActivity.class);
						intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent1);
						break;
					default:
						// キャンセル
						break;
					}
				}
			}
					).show();

			return true;
		case R.id.settings:
			Intent intent2 = new Intent(this, SettingsActivity.class);
			intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent2);
		default:
			return false;
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.getId()==R.id.ListView) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.menu_list, menu);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch(item.getItemId()) {
		case R.id.delete:
			// remove stuff here
			favdatabase.deleteWord(info.id);
			Intent intent1 = new Intent(FavActivity.this, FavActivity.class);
			intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent1);

			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}
}