package com.example.russian;



import com.example.russian.DictionaryProvider;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.provider.SearchRecentSuggestions;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
/* 
 * 三浦脩和
 */
public class MainActivity extends Activity{
	private ListView listView;
	//static DictionaryDatabase dictionarydatabase;
	private TextView textView;
	static ProgressDialog progressDialog;
	Thread thread;
	static boolean stemEnabled;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		listView = (ListView)findViewById(R.id.ListView);	
		textView = (TextView)findViewById(R.id.TextView);
		textView.setText("全"+DictionaryDatabase.numWords+"収録");
		progressDialog = new ProgressDialog(this);
		progressDialog.setTitle("Loading the dictionary...");
		progressDialog.setMessage("Load in progress");
		progressDialog.setCancelable(false);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setMax(DictionaryDatabase.numWords);
		progressDialog.setProgress(0);
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		stemEnabled = sharedPreferences.getBoolean("stemEnabled", true);
		handleIntent(getIntent());
	}


	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		DictionaryProvider.mDictionary = new DictionaryDatabase(this);
		// dictionarydatabase = new DictionaryDatabase(this);
	}

	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			// handles a arch query
			String query = intent.getStringExtra(SearchManager.QUERY);
			showResults(query);
		} else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // handles a click on a search suggestion; launches activity to show word
            Intent wordIntent = new Intent(this, WordActivity.class);
            wordIntent.setData(intent.getData());
            startActivity(wordIntent);
        }
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// Because this activity has set launchMode="singleTop", the system calls this method
		// to deliver the intent if this activity is currently the foreground activity when
		// invoked again (when the user executes a search from this activity, we don't create
		// a new instance of this activity, so the system delivers the search intent here)
		handleIntent(intent);
	}
	/**
	 * Searches the dictionary and displays results for the given query.
	 * @param query The search query
	 */
	private void showResults(final String query) {

		/**Cursor cursor = managedQuery(DictionaryDatabase.CONTENT_URI, null, null,
				new String[] {query}, null);*/

		String[] columns = new String[] {
				BaseColumns._ID,
				DictionaryDatabase.KEY_WORD,
				DictionaryDatabase.KEY_DEFINITION,
				DictionaryDatabase.STEMMED};

		//final Cursor cursor =  dictionarydatabase.getWordMatches(query, columns);

		final Cursor cursor = managedQuery(DictionaryProvider.CONTENT_URI, null, null,
				new String[] {query}, null);

		if (cursor == null) {
			// There are no results
			textView.setText("'"+query+"'"+"での検索結果　0件");
		} else {
			String meta = "'"+ query + "'"+"での検索結果 "+cursor.getCount()+"件";
			textView.setText(meta);
			// Specify the columns we want to display in the result
			String[] from = new String[] { DictionaryDatabase.KEY_WORD,
					DictionaryDatabase.KEY_DEFINITION };

			// Specify the corresponding layout elements where we want the columns to go
			int[] to = new int[] { R.id.word,
					R.id.definition };

			// Create a simple cursor adapter for the definitions and apply them to the ListView
			SimpleCursorAdapter words = new SimpleCursorAdapter(this,
					R.layout.result, cursor, from, to);
			listView.setAdapter(words);

			listView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					// Build the Intent used to open WordActivity with a specific word Uri
					Intent wordIntent = new Intent(getApplicationContext(), WordActivity.class);
					/**cursor.moveToFirst();
					for(int i = 0; i < position;i++){
						cursor.moveToNext();
					}
					int wIndex = cursor.getColumnIndexOrThrow(DictionaryDatabase.KEY_WORD);
					String res = cursor.getString(wIndex);
					wordIntent.putExtra("res",res);*/
					Uri data = Uri.withAppendedPath(DictionaryProvider.CONTENT_URI,
							String.valueOf(id));
					wordIntent.setData(data);
					startActivity(wordIntent);
				}
			});
		}
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.options_menu, menu);
		// Associate searchable configuration with the SearchView
		SearchManager searchManager =
				(SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView =
				(SearchView) menu.findItem(R.id.search).getActionView();
		searchView.setSearchableInfo(
				searchManager.getSearchableInfo(getComponentName()));
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.fav:
			Intent intent = new Intent(this, FavActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			return true;
		case R.id.settings:
			Intent intent2 = new Intent(this, SettingsActivity.class);
			intent2.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent2);
			return true;
		default:
			return false;
		}
	}


	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}


}
